import { Component, OnInit } from '@angular/core';
import { TodoItem } from './todo-item';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.sass']
})
export class TodoListComponent implements OnInit {

  tasks: TodoItem[] = [
    {description: "Cadastrar dados financeiros na planilha", done: true},
    {description: "Pedir sanduiche na quinta-feira", done: false},
    {description: "Terminar o trabalho prático do IGTI", done: false},
  ]
  constructor() { }

  ngOnInit() {
  }
  addTask(value){
    this.tasks.push({
      description: value,
      done: false
    })
  }
  deleteTask(index: number){
    this.tasks.splice(index, 1);
  }
}
