import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tic-tac-toe',
  templateUrl: './tic-tac-toe.component.html',
  styleUrls: ['./tic-tac-toe.component.sass']
})
export class TicTacToeComponent implements OnInit {

  players: string[] = ["X", "O"];
  currentPlayer: string = this.players[0];
  boardStatus: string[][] = [
    ['','',''],
    ['','',''],
    ['','','']
  ];
  winner: string = "";
  constructor() { 

  }


  ngOnInit() {
  }

  nextPlayer(){
    return this.currentPlayer == this.players[0] ? this.players[1] : this.players[0]; 
  }
  restartMatch(){
    this.boardStatus = [
      ['','',''],
      ['','',''],
      ['','','']
    ];
    this.currentPlayer = this.nextPlayer();
    this.winner = "";
  }
  
  hasWinner(player: string){
    for (let index = 0; index < this.boardStatus.length; index++) {
      if(this.boardStatus[index][0] == player && this.boardStatus[index][1] == player
        && this.boardStatus[index][2] == player){
        return true;
      }
    }
    for (let index = 0; index < this.boardStatus.length; index++) {
      if(this.boardStatus[0][index] == player && this.boardStatus[1][index] == player
        && this.boardStatus[2][index] == player){
          return true;
        }
    }
    if(this.boardStatus[0][0] == player &&this.boardStatus[1][2] && this.boardStatus[2][2]){
      return true;
    }
    if(this.boardStatus[0][2] == player &&this.boardStatus[1][2] && this.boardStatus[2][0]){
      return true
    }
    return false;

  }
  validPlay(line: number, column: number){
    if(this.boardStatus[line][column] == ""){
      return true;
    }
    else{
      return false;
    }
  }
  play(line: number, column: number){
    if(this.validPlay(line, column) && this.winner ==""){
      this.boardStatus[line][column] = this.currentPlayer;
      if(this.hasWinner(this.currentPlayer)){
        this.winner = this.currentPlayer;
      }
      this.currentPlayer = this.nextPlayer();
    }
    
      
    
  }
}
