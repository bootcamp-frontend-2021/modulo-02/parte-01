export interface ListItem{
    description: string;
    selected: boolean;
}