import { Component, Input, OnInit } from '@angular/core';
import { ListItem } from './listItem';

@Component({
  selector: 'app-selecao',
  templateUrl: './selecao.component.html',
  styleUrls: ['./selecao.component.sass']
})

export class SelecaoComponent implements OnInit {
  @Input() titulo: string;
  @Input() opcoes: string[];
  itensList: ListItem[] = [];
  @Input() escolhaAte: number;
  itensSelecteds: string = "";
  constructor() { }

  isDone(){
    console.log(this.itensList);
    this.updateSelectedStr()
    return this.itensList.filter(item => item.selected).length >= this.escolhaAte
  }
  updateSelectedStr(){
    const itensCount = this.itensList.filter(item => item.selected).length
    switch (itensCount) {
      case 0:
        this.itensSelecteds = "";
        break;
      case 1:
        this.itensSelecteds = "(" + itensCount + " selecionado)";
        break;  
      default:
        this.itensSelecteds = "(" + itensCount + " selecionados)";
        break;
    }
    this.itensList.filter(item => item.selected).length
  }
  ngOnInit() {
    this.opcoes.forEach(item=>{
      let newListItem: ListItem = {description: item, selected:false}
      this.itensList.push(newListItem)
    })
  }

}
