# Módulo 2 - Angular - Parte 01

# Básico do Typescript

- É uma extensão do `js` adicionando uma tipagem estática (em tempo de compilação)
## Vantagens
- Verificação de erros em tempo de compilação
- Facilidade de manutenção
- Melhor suporte ferramental

## Desvantagens

## Utilidades
- Criação de `interfaces`
```ts
interface Aluno {
    matricula: number,
    nome: string,
    dataNascimento: string,
}
```

- Compatibilidade de tipos. É possível atribuir `aluno2` em uma classe aluno?
```ts
interface Aluno {
    matricula: number,
    nome: string,
    dataNascimento: string,
}

function cadastraAluno(aluno: Aluno){
    
}

let aluno2 = {
    matricula: 132,
    nome: 'Cleitin',
    dataNascimento: '12/05/1994'
    turma: 'A2'
}
cadastraAluno(aluno2);
```

- Utilização de classes
```ts
class Estudante{
    matricula: number,
    nome: string,

    salvar(){

    }
}

let est = new Estudante();
est.salvar();
```

## Data-bindind:
- O `HTML` possibilita a interação com o `ts` de forma direta

## Ng content
Utilização de componentes como conteúdo

## Pipes e impure pipes
Formatações criadas em `.ts` para serem utilizadas direto no html

## FormGroup e FormControl