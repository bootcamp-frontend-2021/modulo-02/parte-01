import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Contact } from '../model/contact';

const baseUrl = 'http://localhost:3000'
@Injectable({
  providedIn: 'root'
})
export class ContactsService {

  constructor(private http: HttpClient) { }

  getContacts(){
    return this.http.get<Contact[]>(`${baseUrl}/contacts?_sort=name`);
  }

  retriveContacts(id: number){
    return this.http.get<Contact[]>(`${baseUrl}/contacts/${id}`);
  }

  createContact(contact: Contact){
    return this.http.post<Contact>(`${baseUrl}/contacts`, contact);
  }

  updateContact(contact: Contact){
    return this.http.put<Contact>(`${baseUrl}/contacts/${contact.id}`, contact);
  }

  deleteContact(id: number){
    return this.http.delete(`${baseUrl}/contacts/${id}`);
  }
}
