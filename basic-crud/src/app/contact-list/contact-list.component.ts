import { Component, OnInit } from '@angular/core';
import { Contact } from '../model/contact';
import { ContactsService } from '../service/contacts.service';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.sass']
})
export class ContactListComponent implements OnInit {
  contactsList: Contact[]
  constructor(private cs: ContactsService) { }

  ngOnInit() {
    this.cs.getContacts().subscribe(contacts =>{
      this.contactsList = contacts;
    })
  }

  deleteContact(contact:Contact){
    this.cs.deleteContact(contact.id).subscribe(() =>{
      const index = this.contactsList.indexOf(contact);
      this.contactsList.splice(index, 1);
    });
  }

}
