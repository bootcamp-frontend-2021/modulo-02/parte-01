import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ContactsService } from '../service/contacts.service';

@Component({
  selector: 'app-contact-create',
  templateUrl: './contact-create.component.html',
  styleUrls: ['./contact-create.component.sass']
})
export class ContactCreateComponent implements OnInit {

  contactForm = new FormGroup({
    name: new FormControl('', Validators.required),
    phone: new FormControl('', Validators.required),
  })

  constructor(private cs: ContactsService, private router: Router) { }

  ngOnInit() {

  }
  createContact(): void{
    const newContact = this.contactForm.value;
    this.cs.createContact(newContact).subscribe(contact =>{
      this.router.navigate(['/contacts']);
    });
  }

}
