import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ContactsService } from '../service/contacts.service';

@Component({
  selector: 'app-contact-update',
  templateUrl: './contact-update.component.html',
  styleUrls: ['./contact-update.component.sass']
})
export class ContactUpdateComponent implements OnInit {
  currentId: number;
  contactForm = new FormGroup({
    name: new FormControl('', Validators.required),
    phone: new FormControl('', Validators.required),
  })
  
  constructor(private cs: ContactsService, private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.paramMap.subscribe(paramMap =>{
      this.currentId = parseInt(paramMap.get('id'));
    })
    this.cs.retriveContacts(this.currentId).subscribe(contact => {
      this.contactForm.reset(contact);
    });
  }

  updateContact(){
    this.cs.updateContact({id:this.currentId, ...this.contactForm.value})
    .subscribe(contact => {
      this.contactForm.reset(contact);
      this.router.navigate(['/contacts']);
    })
  }

}
