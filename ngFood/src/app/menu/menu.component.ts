import { Component, OnInit } from '@angular/core';
import { Product } from '../model/product';
import { MenuService } from '../service/menu.service';
import { OrderService } from '../service/order.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.sass']
})
export class MenuComponent implements OnInit {

  constructor(private ms: MenuService, private os: OrderService) { }
  productList: Product[];

  ngOnInit() {
    this.ms.getMenuList().subscribe(products => {
      this.productList = products.sort((a,b) => (a.categoria > b.categoria) ? 1 : ((b.categoria > a.categoria) ? -1 : 0))
    })
  }
  addToCart(product: Product){
    this.os.addToCart(product)
  }
  get quantidadeTotal(){
    return this.os.getQuantidade();
  }

  get totalPrice(){
    return this.os.getPrecoTotal();
  }
}
