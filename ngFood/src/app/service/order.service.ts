import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Product } from '../model/product';

const baseUrl = 'http://localhost:3000';
interface Pedido{
    produto: Product,
    quantidade: number
}
@Injectable({
  providedIn: 'root'
})

export class OrderService {

  constructor() { }
  shopCart: Pedido[] = [];

  addToCart(product: Product){
    const item = this.shopCart.find(item=> item.produto.descricao === product.descricao);
    if(item){
        item.quantidade++;
    }
    else{
        this.shopCart.push({
            produto: product,
            quantidade: 1
        });
    }
  }
  getQuantidade(){
      let sum = 0;
      this.shopCart.forEach(item =>{
        sum += item.quantidade; 
      })
      return sum;
  }

  getPrecoTotal(){
      let sum = 0;
      this.shopCart.forEach(item =>{
          sum += item.produto.preco * item.quantidade;
      })
      return sum;
  }

  clearShopCart(){
    this.shopCart = [];
  }

}
