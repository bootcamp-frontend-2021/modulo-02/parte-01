import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Product } from '../model/product';

const baseUrl = 'http://localhost:3000';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  constructor(private http: HttpClient) { }

  getMenuList(){
    return this.http.get<Product[]>(`${baseUrl}/cardapio?_sort=descricao`);
  }

}
