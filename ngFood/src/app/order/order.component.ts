import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { OrderService } from '../service/order.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.sass']
})
export class OrderComponent implements OnInit {

  constructor(private os: OrderService, private router: Router) { }

  ngOnInit() {
  }
  getTotalPrice(){
    return this.os.getPrecoTotal();
  }
  clearShopCart(){
    this.os.clearShopCart();
    this.router.navigate(['cardapio']);
  }

}
